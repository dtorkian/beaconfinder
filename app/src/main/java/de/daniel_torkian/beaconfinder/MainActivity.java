package de.daniel_torkian.beaconfinder;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import java.util.HashMap;

public class MainActivity extends Activity implements SensorEventListener {

    // define the display assembly compass picture
    private ImageView image;

    // record the compass picture angle turned
    private float currentDegree = 0f;

    // device sensor manager
    private SensorManager mSensorManager;
    TextView tvHeading;
    TextView beaconValue;

    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    private int REQUEST_ENABLE_BT = 1;


    private boolean mScanning;
    private Handler mHandler;
    private static final long SCAN_PERIOD = 10000;
    private HashMap<String, String> map = new HashMap<>();
    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    Spinner spinner;

    String selectedDevice = null;

    Bitmap bitmap;
    Bitmap circleBitmap;
    Paint paint;
    float oldDistance =0;
    int counter =0;
    float value =0;
    float degree=0;
    int sizeOfMap=0;
    BitmapFactory.Options myOptions;

    int halfWidth;
    int sechsWidth;
    int halfHeight;
    int sechsHeight;
    int acc = 25;
    TextView accValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);

        //
        image = (ImageView) findViewById(R.id.image);
        accValue = (EditText) findViewById(R.id.accText);
        // TextView that will tell the user what degree is he heading
        tvHeading = (TextView) findViewById(R.id.degree);
        beaconValue = (TextView) findViewById(R.id.beaconValue);
        // initialize your android device sensor capabilities
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        spinner = (Spinner) findViewById(R.id.select);


        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "your device dosn't support BLE!", Toast.LENGTH_SHORT).show();
            finish();
        }
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            //int iCurrentSelection = spinner.getSelectedItemPosition();
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.compasshi,myOptions);
               // if (iCurrentSelection != i){
               //     spinner.setSelection(i, true);
             //   }
           //     iCurrentSelection = i;
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });
        myOptions = new BitmapFactory.Options();
        myOptions.inDither = true;
        myOptions.inScaled = false;
        myOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;// important
        myOptions.inPurgeable = true;

        bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.compasshi,myOptions);
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.RED);

        Bitmap workingBitmap = Bitmap.createBitmap(bitmap);
        Bitmap mutableBitmap = workingBitmap.copy(Bitmap.Config.ARGB_8888, true);


        Canvas canvas = new Canvas(mutableBitmap);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, 25, paint);

        //   image.setAdjustViewBounds(true);
     //   image.setImageBitmap(mutableBitmap);
        halfWidth = bitmap.getWidth()/2;
        sechsWidth = bitmap.getWidth()/6;
        halfHeight = bitmap.getHeight()/2;
        sechsHeight = bitmap.getHeight()/6;
        accValue.setText(String.valueOf(acc));
        accValue.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                // you can call or do what you want with your EditText here
                try{
                    acc=Integer.parseInt(accValue.getText().toString());
                }catch(Exception e){

                }


            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        mBluetoothAdapter.startLeScan(mLeScanCallback);



    }

    @Override
    protected void onResume() {
        super.onResume();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // for the system's orientation sensor registered listeners
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // to stop the listener and save battery
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        // get the angle around the z-axis rotated
        degree = Math.round(event.values[0]);

        tvHeading.setText(Float.toString(degree) + " degrees");

        // create a rotation animation (reverse turn degree degrees)
        RotateAnimation ra = new RotateAnimation(
                currentDegree,
                -degree,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f);

        // how long the animation will take place
        ra.setDuration(210);

        // set the animation after the end of the reservation status
        ra.setFillAfter(true);

        // Start the animation
        image.startAnimation(ra);
        currentDegree = -degree;

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // not in use
    }
    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            String scanValue = bytesToHex(scanRecord);
                            if (isBeacon(scanValue)) {
                                String scan = bytesToHex(scanRecord);

                                // find UUID
                                int beginOfUuid = scan.indexOf("000215") + 6;
                                int endOfUuid = beginOfUuid + 32;
                                String uuid = scan.substring(beginOfUuid, endOfUuid);

                                //find Major
                                int beginOfMajor = endOfUuid;
                                int endOfMajor = beginOfMajor + 4;
                                String major = scan.substring(beginOfMajor, endOfMajor);

                                // find minor
                                int beginOfMinor = endOfMajor;
                                int endOfMinor = beginOfMinor + 4;
                                String minor = scan.substring(beginOfMinor, endOfMinor);

                                // find TX
                                int beginOfTX = endOfMinor;
                                int endOfTX = beginOfTX + 2;
                                String tx = scan.substring(beginOfTX, endOfTX);

                                // TX in dB
                                String txDB = calculateDB(tx);

                                // Calculate Distance
                                float distance = getDistance(Integer.toString(rssi), calculateDB(tx));

                                if (!map.containsKey(uuid.substring(15)+major+minor)) map.put(uuid.substring(15)+major+minor, scanValue);
                                if (selectedDevice!=null){
                                    if (spinner.getSelectedItem().toString().equals(uuid.substring(15)+major+minor))
                                    {
                                        if (counter > acc){
                                            beaconValue.setText(getDistanceString(Integer.toString(rssi), calculateDB(tx)));
                                            double bogenmass = Math.toRadians(degree-90);
                                            double cos = Math.cos(bogenmass);
                                            double sin = Math.sin(bogenmass);

                                            if (value>oldDistance){
                                                paint.setColor(Color.RED);
                                                Bitmap newBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
                                                Canvas canvas = new Canvas(newBitmap);
                                                canvas.drawCircle((float) (halfWidth+sechsWidth*cos), (float) (halfHeight+sechsHeight*sin), 3, paint);
                                                bitmap=newBitmap;
                                            } else {
                                                paint.setColor(Color.GREEN);
                                                Bitmap newBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
                                                Canvas canvas = new Canvas(newBitmap);
                                                canvas.drawCircle((float) (halfWidth+sechsWidth*cos), (float) (halfHeight+sechsHeight*sin), 3, paint);
                                                bitmap=newBitmap;
                                            }
                                            oldDistance=value;

                                            Bitmap workingBitmap = Bitmap.createBitmap(bitmap);
                                            Bitmap mutableBitmap = workingBitmap.copy(Bitmap.Config.ARGB_8888, true);


                                            Canvas canvas = new Canvas(mutableBitmap);
                                            canvas.drawCircle(bitmap.getWidth()/2,bitmap.getHeight()/2, (value/counter)*2, paint);

                                            //   image.setAdjustViewBounds(true);
                                            image.setImageBitmap(mutableBitmap);
                                            value=0;
                                            counter=0;

                                        }else{
                                            counter++;
                                            value+=distance;
                                        }

                                    }
                                }


                                final String[] str= map.keySet().toArray(new String[0]);
                                if (sizeOfMap!=map.size()){
                                    ArrayAdapter<String> adp=new ArrayAdapter<String>(getApplicationContext(),
                                            android.R.layout.simple_list_item_1,str);
                                    spinner.setAdapter(adp);
                                    sizeOfMap=map.size();
                                }

                                selectedDevice= spinner.getSelectedItem().toString();

                            }
                        }
                    });
                }
            };


    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public boolean isBeacon(String record) {
        boolean returnValue = false;
        if(record.contains("4C000215")) returnValue = !returnValue;
        return returnValue;
    }

    private static float getDistance(String rssi, String txDB){
        int rssiValue;
        int txValue;
        int absDifference;


            rssiValue = Math.abs(Integer.parseInt(rssi));
            txValue = Math.abs(Integer.parseInt(txDB));
            return Math.abs(rssiValue - txValue);
    }
    private static String getDistanceString(String rssi, String txDB){
        int rssiValue;
        int txValue;
        int absDifference;

        // Apple Distance Criteria
        int immediate = 5; // Closer than 0.5m
        int near = 15; // Between 2m and 0.5m
        int far = 50; // Between 30m and 2m

        try{
            rssiValue = Math.abs(Integer.parseInt(rssi));
            txValue = Math.abs(Integer.parseInt(txDB));
            absDifference = Math.abs(rssiValue - txValue);

            if(absDifference <= immediate){
                return "Immediate (< 0.5m)";
            }else if(absDifference <= near){
                return "Near (between 2m and 0.5m)";
            }else if(absDifference <= far){
                return "Far (between 30m and 2m)";
            }
        }catch(Exception E){
            Log.v("Exception", E.getCause().toString());
        }


        return "Unknown (Out of range)";
    }

    private static String calculateDB(String hex){
        String hexValue =  hex.toLowerCase();
        char[] charArray = hexValue.toCharArray();
        char[] valueArray = {'a', 'b', 'c', 'd', 'e', 'f'};
        int secValue = 0;
        int firstValue = 0;
        try{
            secValue = Integer.parseInt(String.valueOf(charArray[0]));
        } catch(Exception E) {
            for (int i=0; i<6; i++){
                if ((charArray[0])==(valueArray[i])) {
                    secValue=10+i;
                    break;
                }
            }
        }
        try{
            firstValue = Integer.parseInt(String.valueOf(charArray[1]));
        } catch(Exception E) {
            for (int i=0; i<6; i++){
                if ((charArray[1])==(valueArray[i])) {
                    firstValue=10+i;
                    break;
                }
            }
        }
        return String.valueOf(((secValue * 16) + firstValue)-255);
    }







}